<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $yearName, $activeYear, $yearList;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('year')) {
			$init_year   =  active_year();
			$year_name   = $init_year->nama_tahun;
			$active_year = $init_year->kode_tahun;
			$list_year   = list_year()->data;
			$year_data   = [
				'year_name' => $year_name,
				'act_year' => $active_year,
				'list_year' => $list_year
			];
			$this->session->set_userdata('year', $year_data, 7200);
		}
		$this->yearName   = $this->session->userdata('year')['year_name'];
		$this->activeYear = $this->session->userdata('year')['act_year'];
		$this->yearList   = $this->session->userdata('year')['list_year'];
	}

	public function _typeApproval(int $type, &$tableName, &$message)
	{	
		switch ($type) {
			case '1': // Pengajaran/ Pendidikan
				$tableName = 'pengajaran_tambahan_dosen';
				$message   = 'Pengesahan Pengajaran';
				break;
			case '2': // Penelitian
				$tableName = 'penelitian_dosen';
				$message   = 'Pengesahan Penelitian';
				break;
			case '3': // Pengabdian
				$tableName = 'abdimas_dosen';
				$message   = 'Pengesahan Pengabdian';
				break;
			case '4': // Struktural Dosen
				$tableName = 'struktural_dosen';
				$message   = 'Pengesahan Struktural Dosen';
				break;
		}
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */