<?php 

	function dd($message, $is_exit='')
	{
		switch ($is_exit) {
			case 1:
				echo "<pre>";
				print_r ($message);
				echo "</pre>";
				die();
				break;
			
			default:
				echo "<pre>";
				var_dump ($message);
				echo "</pre>";
				die();
				break;
		}
	}

	function PopulateForm()
	{
		$CI = &get_instance();
		$post = array();
		foreach (array_keys($_POST) as $key) {
			$post[$key] = $CI->input->post($key);
		}
		return $post;
	}

	function active_year()
	{
		$http_header = ['x-bkd-key: '.APP_KEY, 'Content-Type: application/json'];
		$hostapi = ENVIRONMENT == 'development' ? DEV_HOST_ENDPOINT : PRO_HOST_ENDPOINT;
		$CI =& get_instance();
		$data = $CI->curl_lib->exec_curl($hostapi.'/api-bkd/active_year', $http_header);
		$decode_data = json_decode($data);
		return $decode_data->data[0];
	}

	function get_teaching_param($code)
	{
		$CI = &get_instance();
		$param = $CI->db->get_where('param_pengajaran_tambahan', ['kode' => $code])->row();
		return $param;
	}

	function guess_academic_year($year, $i)
	{
		$prefix = substr($year, 0, 4);
		$subfix = substr($year, 4, 1);

		$added_smt = $i * 0.5;
		if (($subfix % 2) == 1 && $added_smt < 1) {
			$must_add = 0;
		} elseif (($subfix % 2) == 0 && $added_smt < 1) {
			$must_add = round($added_smt);
		} elseif (($subfix % 2) == 0 && $added_smt > 0.5)  {
			$must_add = ceil($added_smt);
		} else {
			$must_add = floor($added_smt);
		}

		$res_subfix = ($subfix + $i) % 2 == 1 ? 1 : 2;

		$res_prefix = $prefix + $must_add;

		$result = $res_prefix.$res_subfix;
		return $result;
	}

	function list_year()
	{
		$CI =& get_instance();
		$http_header = ['x-bkd-key: '.APP_KEY, 'Content-Type: application/json'];
		$hostapi = ENVIRONMENT == 'development' ? DEV_HOST_ENDPOINT : PRO_HOST_ENDPOINT;
		$years = $CI->curl_lib->exec_curl($hostapi.'/api-bkd/year_list', $http_header);
		return json_decode($years);
	}

	function notohari($day)
	{
		switch ($day) {
			case 1:
				return 'Senin';
				break;

			case 2:
				return 'Selasa';
				break;

			case 3:
				return 'Rabu';
				break;

			case 4:
				return 'Kamis';
				break;

			case 5:
				return 'Jum\'at';
				break;

			case 6:
				return 'Sabtu';
				break;

			case 7:
				return 'Minggu';
				break;
			
			default:
				return '';
				break;
		}
	}

	function TanggalIndo($date, $format = '')
	{
		$BulanIndo = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

		$split = explode('-', $date);

		if ($format == 'd M Y')
			return $split[0] . ' ' . $BulanIndo[(int) $split[1]] . ' ' . $split[2];

		return $split[2] . ' ' . $BulanIndo[(int) $split[1]] . ' ' . $split[0];
	}

	function year_name($year)
	{
		$prefix = substr($year, 0, 4);
		$subfix = substr($year, -1);

		$subfixname = $subfix == 2 ? 'Genap' : 'Ganjil';
		$year_name = $prefix.' / '.$subfixname;
		return $year_name;
	}

	function position_name($pos)
	{
		switch ($pos) {
			case 'SAH':
				return 'Asisten Ahli';
				break;

			case 'TPD':
				return 'Tenaga Pendidik';
				break;

			case 'BIG':
				return 'Guru Besar';
				break;

			case 'LKT':
				return 'Lektor';
				break;
			
			default:
				return 'Lektor Kepala';
				break;
		}
	}

	function lecture_data($userid)
	{
		$CI =& get_instance();
		$http_header = ['x-bkd-key: '.APP_KEY, 'Content-Type: application/json'];
		$hostapi = ENVIRONMENT == 'development' ? DEV_HOST_ENDPOINT : PRO_HOST_ENDPOINT;
		$res = $CI->curl_lib->exec_curl($hostapi.'/api-bkd/lecturer_data', $http_header,true, json_encode(['userid' => $userid]));
		return json_decode($res);
	}

	function lecture_list($query)
	{
		$CI =& get_instance();
		$http_header = ['x-bkd-key: '.APP_KEY, 'Content-Type: application/json'];
		$hostapi = ENVIRONMENT == 'development' ? DEV_HOST_ENDPOINT : PRO_HOST_ENDPOINT;
		$res = $CI->curl_lib->exec_curl($hostapi.'/api-bkd/lecturer_list', $http_header,true, json_encode(['filter' => $query]));
		return json_decode($res);
	}

	function create_url($str='')
	{
		if (stripos($str, "http") or stripos($str, "https") !== false) {
			return $str;
		}else{
			$str = "http://".$str;
			return $str;
		}
	}

	function approval_status_bkd($type, $id_user_group)
	{
		$status = null;
		switch ($type) {
			case '1': // Approval
				if ($id_user_group == 25) { // Approve LPPMP
					$status = 0;
				}

				if ($id_user_group == 21) { // Approve GPM
					$status = 1;
				}

				if ($id_user_group == 26) { // Approve Kaprodi
					$status = 9;
				}

				if ($id_user_group == 27) { // Approve Wakil Dekan 1
					$status = 11;
				}

				if ($id_user_group == 22) { // Approve SPM
					$status = 2;
				}

				if ($id_user_group == 24) { // Approve Dekan
					$status = 3;
				}

				if ($id_user_group == 23) { // Approve Warek I
					$status = 4;
				}

				break;

			case '2': // Revisi
				if ($id_user_group == 25) { // Revisi LPPMP
					$status = 5;
				}

				if ($id_user_group == 21) { // Revisi GPM
					$status = 6;
				}

				if ($id_user_group == 26) { // Revisi Kaprodi
					$status = 10;
				}

				if ($id_user_group == 27) { // Revisi Wakil Dekan 1
					$status = 12;
				}

				if ($id_user_group == 22) { // Revisi SPM
					$status = 7;
				}

				if ($id_user_group == 24) { // Revisi Dekan
					$status = 13;
				}

				if ($id_user_group == 23) { // Revisi Warek 1
					$status = 14;
				}

				break;
		}

		return $status;
	}
	
	function approval_status_text($value) : string
	{
		if (is_null($value)) {
			return "Baru diajukan";
		}elseif ($value == 0) {
			return "Approve By LPPMP";
		}else{
			switch ($value) {
			case (0):
				
				break;

			case 1:
				return "Approve By GPM";
				break;

			case 2:
				return "Approve By SPM";
				break;

			case 3:
				return "Approve By Dekan";
				break;

			case 4:
				return "Approve By Wakil Rektor 1";
				break;

			case 5:
				return "Revisi LPPMP";
				break;

			case 6:
				return "Revisi GPM";
				break;

			case 7:
				return "Revisi SPM";
				break;

			case 8:
				return "Revisi Dosen";
				break;

			case 9:
				return "Approve By Kaprodi";
				break;

			case 10:
				return "Revisi Kaprodi";

			case 11:
				return "Approve By Wakil Dekan";
				break;

			case 12:
				return "Revisi Wakil Dekan";
				break;

			case 13:
				return "Revisi Dekan";
				break;

			case 14:
				return "Revisi Wakil Rektor 1";
				break;
			}
		}		
	}

	function create_log($data=[])
	{
		$table = 'log_transaksi_bkd';
		$CI =& get_instance();

		if (is_multi($data)) {
			$CI->db->insert_batch($table, $data);
		}else{
			$CI->db->insert($table, $data);
		}
	}

	function is_multi($a) {
	    $rv = array_filter($a,'is_array');
	    if(count($rv)>0) return true;
	    return false;
	}

	function label_approval($status){
		if (in_array($status, unserialize(APPROVAL_STATUS))) { // Approval
			return 'success';
		}
		if ($status == 8) {
			return 'info';
		}
		return 'warning'; // Revisi
	}
	
	function icon_approval($status){
		if (in_array($status, unserialize(APPROVAL_STATUS))) { // Approval
			return 'fa-check';
		}
		return 'fa-exclamation-circle'; // Revisi
	}

	/**
	 * lastQ
	 * For show last query executed
	 * @return string
	 */
	function lastQ() : string {
		$CI =& get_instance();
		echo "<pre>";
		print_r ($CI->db->last_query());
		echo "</pre>";
		die();
	}

	/**
	 * Button Action 
	 * Approve || Revisi
	 */
	function button_action($key, $type='', $attr=[])
	{
		$CI =& get_instance();
		$text = "Approve";

		$strAttr = " ";

		if ($CI->userGroup == 26) { // kaprodi
			$text = "Recemendation";
		}


		if (!empty($attr)) {
			foreach ($attr as $k => $value) {
				$strAttr .= " ".$k."=\"".$value."\"";
			}
		}
		
		$str = ' <button 
                class="btn btn-xs btn-primary approve" 
                data-action="'.$key.'"
                data-type="'.$type.'"
                '.$strAttr.'>
                <i class="fa fa-check"></i>&nbsp;'.$text.'
              </button>&nbsp;
              <button 
                class="btn btn-xs btn-warning revisi"
                data-action="'.$key.'"
                data-type="'.$type.'">
                <i class="fa fa-close"></i>&nbsp;Revisi</button>';
		echo htmlspecialchars_decode($str);
	}

	function label_action($status='')
	{
		$str = '<span class="label label-'.label_approval($status).'"><i class="fa '.icon_approval($status).'"></i>&nbsp;'.approval_status_text($status).'</span>';
		echo htmlspecialchars_decode($str);
	}