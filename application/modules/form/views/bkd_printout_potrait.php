<?php

class PDF extends FPDF
{
	public $username, $nidn, $fakultas, $prodi, $jabfung, $tahunakademik, $dekan;

	function __construct($userdata, $dean, $tahunakademik)
	{
		parent::__construct("P","mm", "A4");
		$this->nama = $userdata->nama;
		$this->nidn = $userdata->nidn;
		$this->fakultas = $userdata->fakultas;
		$this->prodi = $userdata->prodi;
		$this->jabfung = $userdata->jabfung;
		$this->tahunakademik = $tahunakademik;
		$this->dekan = $dean->name.' - '.$dean->nidn;
	}

	function Header()
	{
		$this->SetMargins(3, 5 ,0);

		$this->SetFont('Arial','B',10);
		$this->setXY(22,4);
		$this->Cell(160,5,'Formulir Beban Kinerja Dosen Internal (BKD Internal)',0,1,'C');
		$this->Cell(200,5,'Universitas Bhayangkara Jakarta Raya',0,1,'C');

		$this->Ln(2);
		$this->setXY(176,6);
		$this->image(FCPATH.'assets/img/logo-ubj.gif',190,2,14);
		// param for image => <file>,<margin-left>,<margin-top>,<size>

		$this->Ln(17);
		$this->SetFont('Arial','',10);
		$this->Cell(33,5,'Nama',0,0,'L');
		$this->Cell(5,5,' : ',0,0,'L');
		$this->Cell(90,5,$this->nama,0,0,'L');
		$this->Cell(23,5,'Fakultas',0,0,'L');
		$this->Cell(5,5,' : ',0,0,'C');
		$this->Cell(170,5,$this->fakultas,0,0,'L');

		$this->Ln(5);
		$this->SetFont('Arial','',10);
		$this->Cell(33,5,'NIDN',0,0,'L');
		$this->Cell(5,5,' : ',0,0,'L');
		$this->Cell(90,5,$this->nidn,0,0,'L');
		$this->Cell(23,5,'Program Studi',0,0,'L');
		$this->Cell(5,5,' : ',0,0,'C');
		$this->Cell(170,5,$this->prodi,0,0,'L');

		$this->Ln(5);
		$this->SetFont('Arial','',10);
		$this->Cell(33,5,'Jabatan Fungsional',0,0,'L');
		$this->Cell(5,5,' : ',0,0,'L');
		$this->Cell(90,5,$this->jabfung,0,0,'L');
		$this->Cell(23,5,'Semester',0,0,'L');
		$this->Cell(5,5,' : ',0,0,'C');
		$this->Cell(170,5,year_name($this->tahunakademik),0,0,'L');
		$this->Ln(10);
	}

	function Footer()
	{
		$this->SetY(-70);

		date_default_timezone_set('Asia/Jakarta');

		$this->Ln(5);
		$this->SetFont('Arial','B',8);
		$this->Cell(264,1,'',0,0,'C');
		$this->Cell(7,15,'Bekasi, '.TanggalIndo(date('Y-m-d')),0,0,'R');

		$this->Ln(15);
		$this->SetFont('Arial','B',8);
		$this->Cell(20,1,'',0,0,'C');
		$this->Cell(80,1,'Wakil Rektor I',0,0,'L');
		$this->Cell(65,1,'Dekan',0,0,'L');
		$this->Cell(50,1,'Dosen',0,0,'L');


		$this->Ln(20);
		$this->SetFont('Arial','B',8);
		$this->Cell(3,1,'',0,0,'C');
		$this->MultiCell(55,5,'Prof. Drs. Tatang Ary Gumanti, M.Buss, Acc., Ph.D - 0025116604',0,'C');
		
		$this->SetXY(80, -30);
		$this->MultiCell(55,5,$this->dekan,0,'C');

		$this->SetXY(145, -30);
		$this->MultiCell(55,5,$this->nama.' - '.$this->nidn,0,'C');
	}
}

$pdf = new PDF($data, $dean, $this->session->userdata('tahunakademik'));

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','B',8);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->Cell(7,10,'NO','L,T,R,B',0,'C');
$pdf->Cell(100,10,'BIDANG / TUGAS','L,T,R,B',0,'C');
$pdf->Cell(60,5,'WAKTU','L,T,R,B',0,'C');
$pdf->Cell(35,10,'Jumlah SKS','L,T,R,B',0,'C');

$pdf->Ln(5);
$pdf->Cell(108,10,'',0,0,'C');
$pdf->Cell(30,5,'HARI','L,T,R,B',0,'C');
$pdf->Cell(30,5,'JAM','L,T,R,B',0,'C');

$pdf->Ln(5);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(7,5,'1','L,T,R,B',0,'C');
$pdf->Cell(195,5,'Pendidikan dan Pengajaran','L,T,R,B',0,'L');

// constanta for credit total
$creditTotal = 0;
$const = 0;

foreach ($courses as $course) {

	// make constant for decide add new page or no
	$const += 5;

	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,10,'',0,0,'L');
	$pdf->Cell(7,5,'','L,T,R,B',0,'L');
	$pdf->Cell(100,5,$course->nama_mk. ' ('.$course->kode_mk.')','L,T,R,B',0,'L');
	$pdf->Cell(30,5,$course->hari,'L,T,R,B',0,'L');
	$pdf->Cell(30,5,$course->jam_mulai.' - '.$course->jam_selesai,'L,T,R,B',0,'L');
	$pdf->Cell(35,5,$course->sks,'L,T,R,B',0,'C');

	$creditTotal = $creditTotal + $course->sks;
}

foreach ($additional as $aditionals) {
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,5,'',0,0,'C');
	if (strlen($aditionals->komponen) > 55) {
		$aditionalsLen = strlen($aditionals->komponen);

		if ($aditionalsLen > 111) {
			$h = 15;
		} elseif ($aditionalsLen > 56) {
			$h = 10;
		} else {
			$h = 5;
		}

		$pdf->Cell(7,$h,'','LTRB',0,'C');
		$pdf->MultiCell(100,5,'Pengajaran: '.$aditionals->komponen,'LTRB','L',FALSE);
		$pdf->SetXY($pdf->GetX() + 108, $pdf->GetY() - $h);
		$pdf->Cell(30,$h,'','L,T,R,B',0,'L');
		$pdf->Cell(30,$h,'','L,T,R,B',0,'L');
		$pdf->Cell(35,$h,$aditionals->sks,'L,T,R,B',0,'C');
		// if cell height more than 5, give 5mm new line
		if ($h > 10) {
			$pdf->Ln(10);
		} else {
			$pdf->Ln(5);
		}
	} else {
		$pdf->Cell(7,5,'','LTRB',0,'C');
		$pdf->Cell(100,5,'Pengajaran: '.$aditionals->komponen,'LTRB',0,'L');
		$pdf->Cell(30,5,'','L,T,R,B',0,'L');
		$pdf->Cell(30,5,'','L,T,R,B',0,'L');
		$pdf->Cell(35,5,$aditionals->sks,'L,T,R,B',0,'C');
	}

	$creditTotal = $creditTotal + $aditionals->sks;
}

$pdf->Ln(5);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(7,5,'2','L,T,R,B',0,'C');
$pdf->Cell(195,5,'Penelitian dan Pengabdian Kepada Masyarakat','L,T,R,B',0,'L');

$currentRow = count($courses) + count($additional);

foreach ($research as $rsc) {

	$currentRow += 1;

	if ($currentRow > 12) {
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetMargins(3, 5 ,0);
	}

	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,10,'',0,0,'C');

	if (strlen($rsc->judul) > 175) {
		$height = 20;
	} elseif (strlen($rsc->judul) > 111) {
		$height = 15;
	} elseif (strlen($rsc->judul) > 50) {
		$height = 10;
	} elseif (strlen($rsc->judul) <= 50) {
		$height = 5;
	}

	if (strlen($rsc->judul) > 60) {
		$pdf->Cell(7,$height,'','L,T,R,B',0,'C');
		$pdf->MultiCell(100,5,'Penelitian: '.$rsc->judul,'LTRB','L',FALSE);
		$pdf->SetXY($pdf->GetX() + 108, $pdf->GetY() - $height);
		$pdf->Cell(30,$height,'','L,T,R,B',0,'L');
		$pdf->Cell(30,$height,'','L,T,R,B',0,'L');
		$pdf->Cell(35,$height,$rsc->sks,'L,T,R,B',0,'C');
		$pdf->Ln($height-5);
	} else {
		$pdf->Cell(7,$height,'','L,T,R,B',0,'C');
		$pdf->Cell(100,$height,'Penelitian: '.$rsc->judul,'LTRB','L',FALSE);
		$pdf->Cell(30,$height,'','L,T,R,B',0,'L');
		$pdf->Cell(30,$height,'','L,T,R,B',0,'L');
		$pdf->Cell(35,$height,$rsc->sks,'L,T,R,B',0,'C');
	}

	$creditTotal = $creditTotal + $rsc->sks;
}

foreach ($devotion as $dev) {

	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,10,'',0,0,'C');

	// add new page if number of row > 25
	if ($currentRow < 25) {
		$currentRow += 1;

		if ($currentRow > 25) {
			$pdf->AliasNbPages();
			$pdf->AddPage();
			$pdf->SetMargins(2, 5 ,0);
		}
	}

	if (strlen($dev->program) > 175) {
		$hgt = 20;
	} elseif (strlen($dev->program) > 111) {
		$hgt = 15;
	} elseif (strlen($dev->program) > 50) {
		$hgt = 10;
	} elseif (strlen($dev->program) <= 50) {
		$hgt = 5;
	}

	if (strlen($dev->program) > 60) {
		$pdf->Cell(7,$hgt,'','L,T,R,B',0,'C');
		$pdf->MultiCell(100,5,'Pengabdian: '.$dev->program,'LTRB','L',FALSE);
		$pdf->SetXY($pdf->GetX() + 108, $pdf->GetY() - $hgt);
		$pdf->Cell(30,$hgt,'','L,T,R,B',0,'L');
		$pdf->Cell(30,$hgt,'','L,T,R,B',0,'L');
		$pdf->Cell(35,$hgt,$dev->sks,'L,T,R,B',0,'C');
		$pdf->Ln(($hgt-5));
	} else {
		$pdf->Cell(7,5,'','L,T,R,B',0,'C');
		$pdf->Cell(100,5,'Pengabdian: '.$dev->program,'LTRB',0,'L');
		$pdf->Cell(30,5,'','L,T,R,B',0,'L');
		$pdf->Cell(30,5,'','L,T,R,B',0,'L');
		$pdf->Cell(35,5,$dev->sks,'L,T,R,B',0,'C');
	}
	$creditTotal = $creditTotal + $dev->sks;
}

$pdf->Ln(5);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(7,5,'3','L,T,R,B',0,'C');
$pdf->Cell(195,5,'Lain-lain','L,T,R,B',0,'L');

foreach ($others as $other) {
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,10,'',0,0,'C');
	$pdf->Cell(7,5,'','L,T,R,B',0,'C');
	$pdf->Cell(100,5,$other->jabatan,'L,T,R,B',0,'L');
	$pdf->Cell(30,5,'','L,T,R,B',0,'L');
	$pdf->Cell(30,5,'','L,T,R,B',0,'L');
	$pdf->Cell(35,5,$other->sks,'L,T,R,B',0,'C');

	$creditTotal = $creditTotal + $other->sks;
}

$pdf->Ln(5);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(167,5,'Total','L,T,R,B',0,'C');
$pdf->Cell(35,5,$creditTotal,'L,T,R,B',0,'C');

$pdf->Output('Kartu_BKD_'.date('ymd_his').'.PDF','I');
