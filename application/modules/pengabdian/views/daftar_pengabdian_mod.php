<?php 
$edit_btn_disabled = '';
$tooltip_edit_btn  = 'edit!';
$rm_btn_disabled   = true;
$tooltip_rm_btn    = 'Hapus!';

?>

<table id="example4" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>No</th>
      <th>Jenis</th>
      <th>Program</th>
      <th>Peran/Ketegori</th>
      <th>Note</th>
      <th>SKS</th>
      <th width="90" style="text-align: center;">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1; foreach ($dev as $devs) : ?>
      <tr>
        <td><?= $no ?></td>
        <td><?= $devs->abdimas ?></td>
        <td><?= $devs->program ?></td>
        <td><?= empty($devs->nama) ? '-' : $devs->nama ?></td>
        <td><?= $devs->note ?></td>
        <td><?= $devs->sks ?></td>
        <td>
         <?php if (is_null($devs->status)): ?>
            <span data-toggle="tooltip" title="<?= $tooltip_edit_btn ?>">
              <button 
                class="btn btn-sm bg-purple" 
                data-toggle="modal" 
                data-target="#myModal"
                <?= $edit_btn_disabled ?>
                onclick="edit(<?= $devs->id ?>)">
                <i class="fa fa-pencil"></i>
              </button>
            </span>
            <a 
              href="<?= $rm_btn_disabled ? base_url('remove-dev/'.$devs->id) : 'javascript:void(0);' ?>" 
              <?php if ($rm_btn_disabled) : ?>onclick="return confirm('Anda yakin ingin menghapus data ini?')"<?php endif; ?>
              class="btn btn-sm bg-navy" 
              data-toggle="tooltip" 
              <?= !$rm_btn_disabled ? 'disabled' : ''; ?>
              title="<?= $tooltip_rm_btn ?>">
              <i class="fa fa-trash"></i>
            </a>
          <?php elseif (in_array($devs->status, unserialize(REVITION_STATUS))): //Revisi ?>
            <span data-toggle="tooltip" title="<?= $tooltip_edit_btn ?>">
              <button 
                type="button" 
                class="btn btn-warning" 
                data-toggle="modal" 
                data-target="#myModal"
                <?= $edit_btn_disabled ?>
                onclick="edit(<?= $devs->id ?>)">
                <i class="fa fa-pencil"></i>
              </button>
            </span>
         <?php endif ?>
        </td>
      </tr>
    <?php $no++; endforeach; ?>
  </tbody>
</table>

<script>
  $('#example4').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  });

  $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
</script>