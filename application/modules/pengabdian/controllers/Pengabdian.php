<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengabdian extends MY_Controller {

    public $listYear;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('bkd_session')) {
			redirect('auth','refresh');
		}
		$this->userid = $this->session->userdata('bkd_session')['userid'];
		$this->load->model('pengabdian/devotion_model','dev');
		$this->load->library('cart');
        $this->_generateYearList($this->listYear);
	}

	public function index()
	{
		$this->session->unset_userdata('devotions');
        $this->session->set_userdata('devotions',[]);

        $data['years'] = $this->listYear;
		$data['devotionType'] = $this->db->get('abdimas')->result();
		$data['pagename'] = 'Pengabdian';
		$data['page'] = 'pengabdian_v';
		$this->load->view('template/template', $data);
	}

	public function load_list_page()
	{
		$data['years'] = $this->yearList;
		$data['dev'] = $this->dev->list_all($this->userid, $this->activeYear);
		$this->load->view('daftar_pengabdian', $data);
	}

	/**
	 * Get devotion program by its devotion
	 * @param string $id
	 * @return void
	 */
	public function get_devotion_program($code)
	{
		$programs = $this->dev->get_devotion_program($code);

		$option = "<option value=\"\" disabled=\"\" selected=\"\"></option>";
		foreach ($programs as $program) {
			$option .= "<option value='".$program->kode_program."'>".$program->program."</option>";
		}

		echo $option;
	}

	/**
	 * Get devotion param
	 * @param string $id
	 * @return void
	 */
	public function get_devotion_param($code)
	{
		$params = $this->dev->get_devotion_param_weight($code);

		if (count($params) == 0) {
			$sks = $this->dev->devotion_program($code)->sks;
			echo $sks;
		} else {
			$option = "<option value=\"\" disabled=\"\" selected=\"\"></option>";
			foreach ($params as $param) {
				$option .= "<option value='".$param->kode."'>".$param->nama."</option>";
			}

			echo $option;	
		}
	}

	/**
	 * Set devotion credit
	 * @param string $id
	 * @return void
	 */
	public function set_devotion_credit($code="",$param="")
	{
		$credit = $this->dev->get_devotion_credit($code,$param);
		echo $credit;
	}

	/**
	 * Load temporary devotion table
	 * 
	 * @return void
	 */
	public function load_devotion_temp_table()
    {
        $data['team'] = $this->cart->contents();   
        $this->load->view('temp_table_devotion',$data); 
    }

    /**
	 * Set temporary devotion data
	 * 
	 * @return void
	 */
	public function temporaryDevotion()
	{
        extract(PopulateForm());

		$ses   = $this->session->userdata('devotions');
		$cp    = count($ses);
		$param = isset($devParam) ? $devParam : NULL ;

        if (!$ses) {
            $data = array(
                    $cp => array(
						'nid'           => $this->userid,
						'type'          => $devotion,
						'program'       => $devProgram, 
						'tahunakademik' => $tahunakademik, 
						'param'         => $param, 
						'sks'           => $devCredit
                    )
                );
            $this->session->set_userdata('numberof_array',$cp);     

            $arr = $ses + $data;
            $count = $cp+1;
            $this->session->unset_userdata('devotions');
            $this->session->set_userdata('devotions',$arr);
            $this->session->set_userdata('numberof_array',$count);

         } else {
            $i_arr = $this->session->userdata('numberof_array');
            $data = array(
                $i_arr => array(
                    'nid'           => $this->userid,
					'type'          => $devotion,
					'program'       => $devProgram, 
					'tahunakademik' => $tahunakademik, 
					'param'         => $param, 
					'sks'           => $devCredit
                )
            );     

            $arr = $ses + $data;
            $count = $i_arr+1;
            $this->session->unset_userdata('devotions');
            $this->session->unset_userdata('numberof_array');
            
            $this->session->set_userdata('devotions',$arr);
            $this->session->set_userdata('numberof_array',$count);
         
    	}
	}

	/**
	 * Remove devotion from temporary data
	 * @param int $id
	 * @return void
	 */
	public function delete_temp_devotion($id)
	{
        $arr = $this->session->userdata('devotions');

        unset($arr[$id]);
        
        $this->session->unset_userdata('devotions');
        $this->session->set_userdata('devotions',$arr);
    }

    /**
     * Store devotion
     * 
     * @return void
     */
    public function add_devotion()
    {
    	extract(PopulateForm());

    	$programAmount = count($program);
    	for ($i = 0; $i < $programAmount; $i++) {
    		$devotionList[] = [
                '_key'          => $this->_generateRandomString(),
				'nid'           => $this->userid,
				'tahunakademik' => $tahunakademik[$i],
				'type'          => $type[$i],
				'program'       => $program[$i],
				'param'         => $param[$i],
				'bobot_sks'     => $sks[$i],
				'created_at'    => date('Y-m-d H:i:s')
    		];
    	}
    	$this->_dev_input_validation($devotionList);
    	$this->db->insert_batch('abdimas_dosen', $devotionList);
    	$this->session->set_flashdata('success', 'Data pengabdian berhasil ditambahkan!');
    	redirect('pengabdian');
    }

    /**
     * Validation for devotion. In a semester, only 3 devotions allowed.
     * 
     * @return void
     */
    protected function _dev_input_validation($dev_manifest)
    {
    	$dev_amount = $this->db->get_where("abdimas_dosen",
            [
        		'nid' => $this->userid, 
        		'tahunakademik' => $this->activeYear,
                'deleted_at IS NULL' => NULL
            ]
    	)->num_rows();

        $total_dev_with_newdata = count($dev_manifest) + $dev_amount;

    	if ($dev_amount > 2 OR $total_dev_with_newdata > 3) {
    		$this->session->set_flashdata('fail', 'Gagal menyimpan data! Jumlah pengabdian melebihi batas jumlah per semester!');
    		redirect('pengabdian','refresh');
    	}
        
    	return;
    }

    /**
     * Load detail devotion to edit
     * @param int $id
     * @return void
     */
    public function edit($id)
    {
        $data['years'] = $this->listYear;
    	$data['dev'] = $this->dev->dev_detail($id);
    	$this->load->view('dev_modal_edit', $data);
    }

    /**
     * Update devotion
     * 
     * @return void
     */
    public function update()
    {
    	extract(PopulateForm());

    	$param = isset($devParamEdit) ? $devParamEdit : NULL;

    	$data = [
			'program'       => $devProgramEdit,
			'param'         => $param,
			'bobot_sks'     => $devCreditEdit,
            'tahunakademik' => $tahunakademik,
			'updated_at'    => date('Y-m-d H:i:s')
    	];

        $before = $this->dev->dev_detail($id);
        
        if (!empty($before->status)) {
            $data['status'] = 8;
        }
        
    	$this->db->update('abdimas_dosen', $data, ['id' => $id]);
    	$this->session->set_flashdata('success', 'Data pengabdian berhasil diubah!');
    	redirect('pengabdian');
    }

    /**
     * Remove devotion by update deleted_at column
     * @param int $id
     * @return void
     */
    public function remove($id)
    {
    	$this->_is_dev_exist($id);
        $this->_is_has_report($id);

    	$this->db->update('abdimas_dosen', ['deleted_at' => date('Y-m-d H:i:s')], ['id' => $id]);
    	$this->session->set_flashdata('success', 'Data pengabdian berhasil dihapus!');
    	redirect('pengabdian');
    }

    /**
     * Check whether data is exist or no by its id
     * @param int $id
     * @return void
     */
    private function _is_dev_exist($id)
    {
    	$is_exist = $this->db->get_where('abdimas_dosen', ['id' => $id])->num_rows();
    	if ($is_exist == 0) {
    		$this->session->set_flashdata('fail', 'Gagal menghapus data! Data pengabdian tidak ditemukan!');
    		redirect('pengabdian','refresh');
    	}
    	return;
    }

    /**
     * Check whether report is exist or no by its id
     * @param int $id
     * @return void
     */
    private function _is_has_report($id)
    {
        $get_key = $this->db->get_where('abdimas_dosen', ['id' => $id])->row()->_key;
        $is_exist = $this->db->get_where('bukti_pengabdian', ['_key' => $get_key]);
        if ($is_exist->num_rows() > 0 && is_null($is_exist->row()->deleted_at)) {
            $this->session->set_flashdata('fail', 'Tidak dapat menghapus data! Data pengabdian telah dilaporkan selesai!');
            redirect('pengabdian','refresh');
        }
        return;
    }

    /**
     * Get devotion by its year
     * @param string $year
     * @return void
     */
    public function dev_on_year($year)
    {
        $data['year'] = $year;
    	$data['dev'] = $this->dev->list_all($this->userid, $year);
		$this->load->view('daftar_pengabdian_mod', $data);
    }

    /**
     * Generate random string
     * 
     * @return string
     */
    protected function _generateRandomString() : string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Generate list year by active year - 3
     * @param  Object &$list 
     * @return Object year list
     */
    function _generateYearList(&$list)
    {
        $list = $this->yearList;
        $keys = array_search($this->activeYear, array_column($list, 'kode'));
        $list = array_splice($list, $keys - 3);
    }
}

/* End of file Pengabdian.php */
/* Location: ./application/modules/pengabdian/controllers/Pengabdian.php */