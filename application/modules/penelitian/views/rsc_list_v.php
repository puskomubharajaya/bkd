<section class="content">
  <div class="row">
    <div class="col-xs-12 col-lg-12">
      <div class="form-horizontal">
        <div class="form-group">
          <label for="" class="control-label col-xs-2">Tahun Akademik</label>
          <div class="col-md-8 col-xs-12">
            <select name="year-choice" id="year-choice" class="form-control">
              <option value="" disabled="" selected="">-- Pilih Tahun Ajaran --</option>
              <?php foreach ($years as $key => $value): ?>
                <option value="<?= $value->kode?>" <?= $value->kode == $this->activeYear ? 'selected="true"' : ''?> ><?= $value->tahun_akademik?></option>
              <?php endforeach ?>
            </select>
          </div>
        </div>
      </div>
      <div id="table-show-here">
        <table id="example3" class="table table-bordered table-hover">
          <thead>
              <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Program</th>
                <th>Kegiatan</th>
                <th>Catatan</th>
                <th width="130" style="text-align: center;">Aksi</th>
              </tr>
            </thead>
            <tbody>

              <?php $no=1; foreach ($rsc_list as $rsc) : ?>
                <tr>
                  <td><?= $no ?></td>
                  <td><?= $rsc->judul ?></td>
                  <td><?= $rsc->program ?></td>
                  <td><?= $rsc->kegiatan ?></td>
                  <td><?= $rsc->note ?></td>
                  <td>
                    <span data-toggle="tooltip" title="Detail">
                      <button 
                        class="btn btn-sm bg-olive" 
                        data-toggle="modal" 
                        data-target="#myModal" 
                        onclick="load_detail(<?= $rsc->id ?>)">
                        <i class="fa fa-list"></i>
                      </button>
                    </span>
                    <?php if (in_array($rsc->status, unserialize(REVITION_STATUS))): ?>
                        <span data-toggle="tooltip" title="Edit">
                          <button 
                            class="btn btn-sm bg-purple" 
                            data-toggle="modal" 
                            data-target="#myModal"
                            onclick="edit(<?= $rsc->id ?>)">
                            <i class="fa fa-pencil"></i>
                          </button>
                        </span>
                        <a 
                          href="<?= base_url('remove-rsc/'.$rsc->key) ?>" 
                          onclick="return confirm('Anda yakin ingin menghapus data ini?')" 
                          class="btn btn-sm bg-navy" 
                          data-toggle="tooltip" 
                          title="Delete">
                          <i class="fa fa-trash"></i>
                        </a>
                    <?php elseif (is_null($rsc->status)): ?>
                        <a 
                          href="<?= base_url('remove-rsc/'.$rsc->key) ?>" 
                          onclick="return confirm('Anda yakin ingin menghapus data ini?')" 
                          class="btn btn-sm bg-navy" 
                          data-toggle="tooltip" 
                          title="Delete">
                          <i class="fa fa-trash"></i>
                        </a>
                    <?php endif ?>
                  </td>
                </tr>
              <?php $no++; endforeach; ?>
            </tbody>       
        </table>
      </div>
    </div>
  </div>
</section>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content" id="content">
      
    </div>
  </div>
</div>

<script>
  $('#example3, #example4').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  });
  
  $('#year-choice').change(function() {
    $('#table-show-here').html("<img src='<?= base_url('assets/img/loading.gif')?>' alt='loading...' style=\"width:20%;margin-left:40%;\" />");
    $('#table-show-here').load('<?= base_url('penelitian-pertahun/') ?>' + $(this).val());
  });
  
  $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});
  });

  function load_detail(id) {
    $('#content').load('<?= base_url('rsc-detail/') ?>' + id)
  }

  function edit(id) {
    $('#content').load('<?= base_url('edit/') ?>' + id)
  }
</script>