<?php 
// $is_enable_to_act = $year == $this->activeYear ? true : false; 
$is_enable_to_act = true; 
// $is_form_enable = $is_enable_to_act ? '' : 'disabled=""';
$is_form_enable = $is_enable_to_act ? '' : 'disabled=""';
$is_show_delete_confirm = $is_enable_to_act 
                            ? "onclick=\"return confirm('Anda yakin ingin menghapus data ini?')\"" 
                            : "onclick=\"alert('Tidak dapat menghapus data di tahun lampau!')\"";
?>
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>No</th>
      <th>Judul</th>
      <th>Program</th>
      <th>Kegiatan</th>
      <th width="130" style="text-align: center;">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php if (count($rsc_list) == 0) : ?>
      <tr>
        <td colspan="5"><i>No data available in table</i></td>
      </tr>
    <?php else : ?>
      <?php $no=1; foreach ($rsc_list as $rsc) : ?>
        <?php $is_dead_link = $is_enable_to_act ? base_url('remove-rsc/'.$rsc->key) : 'javascript:void(0)' ?>
        <tr>
          <td><?= $no ?></td>
          <td><?= $rsc->judul ?></td>
          <td><?= $rsc->program ?></td>
          <td><?= $rsc->kegiatan ?></td>
          <td>
            <span data-toggle="tooltip" title="Detail">
              <button 
                class="btn btn-sm bg-olive" 
                data-toggle="modal" 
                data-target="#myModal" 
                onclick="load_detail(<?= $rsc->id ?>)">
                <i class="fa fa-list"></i>
              </button>
            </span>
            <?php if (in_array($rsc->status, unserialize(REVITION_STATUS))): ?>
                <span data-toggle="tooltip" title="Edit">
                  <button 
                    class="btn btn-sm bg-purple" 
                    data-toggle="modal" 
                    data-target="#myModal"
                    <?= $is_form_enable ?>
                    onclick="edit(<?= $rsc->id ?>)">
                    <i class="fa fa-pencil"></i>
                  </button>
                </span>
                <a 
                  href="<?= $is_dead_link ?>" 
                  <?= $is_show_delete_confirm  ?>
                  <?= $is_form_enable ?>
                  class="btn btn-sm bg-navy" 
                  data-toggle="tooltip" 
                  title="Delete">
                  <i class="fa fa-trash"></i>
                </a>
            <?php elseif (is_null($rsc->status)): ?>
                <a 
                  href="<?= $is_dead_link ?>" 
                  <?= $is_show_delete_confirm  ?>
                  <?= $is_form_enable ?>
                  class="btn btn-sm bg-navy" 
                  data-toggle="tooltip" 
                  title="Delete">
                  <i class="fa fa-trash"></i>
                </a>
            <?php endif ?>
          </td>
        </tr>
      <?php $no++; endforeach; ?>
    <?php endif; ?>
  </tbody>        
</table>