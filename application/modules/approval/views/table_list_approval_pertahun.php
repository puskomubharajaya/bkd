<!-- Content Table -->
<button class="btn btn-primary btn-sm approve-all" <?php echo $disabled ?> data-type="<?php echo $data_type ?>"><i class="fa fa-check-square-o"></i>&nbsp;Approve All</button>
<table class="table table-bordered" id="example3" style="width:100%; margin-top: 15px !important">
  <thead>
    <tr>
      <th>No</th>
      <th>Dosen</th>
      <th>Komponen</th>
      <th>Alamat URL</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1; foreach ($teaches as $teach) : 
    $dosen = lecture_data($teach->nid)->data;
    $attr = "";

    if ($this->userGroup == 26) { //Kaprodi
      if (is_null($teach->status) || $teach->status == 8) 
        $attr = "data-code=".$teach->kode_transaksi;
    }elseif ($this->userGroup == 21) { //GPM
      if ($teach->status == 9) 
        $attr = "data-code=".$teach->kode_transaksi;
    }elseif ($this->userGroup == 27) { //Wakil Dekan 1
      if ($teach->status == 1) 
        $attr = "data-code=".$teach->kode_transaksi;
    }elseif ($this->userGroup == 22) { //SPM
      if ($teach->status == 11) 
        $attr = "data-code=".$teach->kode_transaksi;
    }elseif ($this->userGroup == 23) { //REKTORAT
      if ($teach->status == 3) 
        $attr = "data-code=".$teach->kode_transaksi;
    }elseif ($this->userGroup == 24) { //DEKAN
      if ($teach->status == 2) 
        $attr = "data-code=".$teach->kode_transaksi;
    }
    ?>
      <tr class="action-all" <?php echo $attr ?> >
        <td><?= $no ?></td>
        <td><?php echo $dosen->nama. " - ". $dosen->nidn ?></td>
        <td><?= $teach->komponen ?></td>
        <td id="column-flag-<?= $teach->kode_transaksi ?>" style="vertical-align: middle;">
          <a href="<?php echo create_url($teach->tsc_url) ?>" target="new" class="btn btn-xs btn-primary"><?php echo create_url($teach->tsc_url);?></a>
        </td>

        <td id="column-action-<?= $teach->kode_transaksi ?>">
         <?php 
          if ($this->userGroup == 26) { //Ka Prodi
            if(is_null($teach->status) || $teach->status == 8) {
              button_action($teach->kode_transaksi, $teach->data_type, ['data-uid' => $this->userGroup]);
            }elseif (!is_null($teach->status)) {
              label_action($teach->status);
            }
          }elseif ($this->userGroup == 21){ // GPM 
            if($teach->status == 9){ // 9 = Rekomendasi Kaprodi  
              button_action($teach->kode_transaksi, $teach->data_type);
            }else{
              label_action($teach->status);
            }
          }elseif($this->userGroup == 22){ // SPM 
            if($teach->status == 11){ // 11 = Approve Wakil Dekan 1
              button_action($teach->kode_transaksi, $teach->data_type);
            }elseif (!is_null($teach->status)) {
              label_action($teach->status);
            }
          }elseif($this->userGroup == 23) { // REKTORAT
            if($teach->status == 3){
              button_action($teach->kode_transaksi, $teach->data_type, ['data-uid' => $this->userGroup]);
            }else {
              label_action($teach->status);
            }
          }elseif($this->userGroup == 24) { // DEKAN
            if($teach->status == 2){
              button_action($teach->kode_transaksi, $teach->data_type, ['data-uid' => $this->userGroup]);
            }else {
              label_action($teach->status);
            }
          }elseif ($this->userGroup == 27) { // Wakil Dekan 1
            if($teach->status == 1){
              button_action($teach->kode_transaksi, $teach->data_type, ['data-uid' => $this->userGroup]);
            }elseif (!is_null($teach->status)) {
              label_action($teach->status);
            }
          }
        ?>
        </td>

      </tr>
    <?php $no++; endforeach; ?>
  </tbody>
</table>
<!-- Content Table -->
<script>
$(document).ready(function () {
  $('#example3').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false,
    'scrollX': true,
  })

  jQuery.fn.tagNameLowerCase = function() {
    return this.prop("tagName").toLowerCase();
  };

  $('[data-toggle="tooltip"]').tooltip({ trigger: 'hover' })
})

$(".approve-all").on("click", function (e) {
    let arr = []
    let uid = 0;
    $("#example3 tbody tr").each( (index, tr) => {
      if (typeof tr["attributes"]["data-code"] !== 'undefined') {
        let code = tr["attributes"]["data-code"]["value"];
         let tag = $('#column-action-'+code).children().tagNameLowerCase()

        if (tag !== "span") {
          arr.push(code)
        }
      }

    })

    if (arr.length == 0) { 
      alert("Data tidak ditemukan"); 
      return;
    }

    claim($(this).attr("data-type"), arr, null, true)
  })
  
  $(".approve").on('click', function (e) {
    let message = null
    let uid = 0;
    if (typeof $(this).attr("data-uid") !== 'undefined') {
      uid++
    }

    if (uid > 0) {
      message = prompt("Catatan untuk dosen")
    }
    
    claim($(this).attr("data-type"), $(this).attr("data-action"), message, true)
  })

  $(".revisi").on("click", function(e){
    let r = confirm("Yakin ingin merivisi ?")
    if (r) {
      let message = prompt("Catatan untuk dosen ")
      claim($(this).attr("data-type"), $(this).attr("data-action"),message)
    }
  })

  function claim(type,arg, note = null, approve=null) {
    $.ajax({
      method: 'post',
      url: '<?= base_url($_approveURL) ?>',
      data:{ type: type, code: arg, note: note, isApprove: approve},
      beforeSend: function() {
        if (Array.isArray(arg)) {
          for (var i = 0; i < arg.length; i++) {
            $('#column-action-'+arg[i]).html(`<img src="<?= base_url('assets/img/0.gif') ?>" width="10%" />`)
          }
        }else{
          $('#column-action-'+arg).html(`<img src="<?= base_url('assets/img/0.gif') ?>" width="10%" />`)
        }
      },
      success: function(res) {
        res = JSON.parse(res);
        if (res.code !== 200) { alert(res.message); window.location.reload(true); return;}
        if (Array.isArray(arg)) {
          if (res.error) { alert(res.message + " Error : "+res.error)}
          
          for (var i = 0; i < arg.length; i++) {
            $('#column-action-'+arg[i]).html(res.label)
          }
        }else{
         $('#column-action-'+arg).html(res.label)
        }
        
        if (res.disable) {
          $(".approve-all").attr("disabled", true) 
        }
      }
    })
  }
</script>