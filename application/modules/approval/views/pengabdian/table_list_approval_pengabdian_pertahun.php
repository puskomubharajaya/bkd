<button class="btn btn-primary btn-sm approve-all" <?php echo $disabled ?> data-type="<?php echo $data_type ?>"><i class="fa fa-check-square-o"></i>&nbsp;Approve All</button>
<table class="table table-bordered display" id="example3" style="width:100%;margin-top: 15px !important">
  <thead>
    <tr>
      <th>No</th>
      <th>Dosen</th>
      <th>Judul</th>
      <th>Program</th>
      <th>Keterangan</th>
      <th style="width: 120px !important;">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1; foreach ($teaches as $rsc) :
      $dosen = lecture_data($rsc->nid)->data;
      $attr = "";

      if ($this->userGroup == 26 ) { //Kaprodi
       if ($rsc->status == 8 || is_null($rsc->status)) {
         $attr = "data-code=".$rsc->key;
       }
      }elseif ($this->userGroup == 21) { //GPM
        if ($rsc->status == 9 ) // Approve By Kaprodi
          $attr = "data-code=".$rsc->key; 
      }elseif ($this->userGroup == 27) { //Wakil Dekan 1
        if ($rsc->status == 0 )  
          $attr = "data-code=".$rsc->key; 
      }elseif ($this->userGroup == 22) { //SPM
        if ($rsc->status == 11) 
          $attr = "data-code=".$rsc->key;
      }elseif ($this->userGroup == 23) { //REKTORAT
        if ($rsc->status == 3) 
          $attr = "data-code=".$rsc->key;
      }elseif ($this->userGroup == 24) { //DEKAN
        if ($rsc->status == 2) 
          $attr = "data-code=".$rsc->key;
      }elseif ($this->userGroup == 25) { //LPPMP
        if($rsc->status == 1)
          $attr = "data-code=".$rsc->key;
      }
    ?>
      <tr class="action-all" <?php echo $attr ?> >
        <td><?= $no ?></td>
        <td><?php echo $dosen->nama. " - ". $dosen->nidn ?></td>
        <td><?= $rsc->judul ?></td>
        <td><?= $rsc->program ?></td>
        <td style="vertical-align: middle;">
          <?= '<a href="#"   
                type="button" 
                data-target="#myInfo"
                data-toggle="modal"
                data-key="'.$rsc->key.'"
                class="btn btn-xs btn-info info-document"><i class="fa fa-exclamation-circle"></i>&nbsp;Info Dokumen</a>' ?>
        </td>
        <td class="column-action-<?php echo $rsc->key?>" style="vertical-align: middle;" >
          <?php
          // <!-- Kaprodi -->
            if ($this->userGroup == 26){ 
              if (is_null($rsc->status) || $rsc->status == 8)
                button_action($rsc->key, $data_type, ["data-uid" => $this->userGroup]);
              else
                label_action($rsc->status);
            }
            // <!-- GPM -->
            elseif($this->userGroup == 21){
              if ($rsc->status == 9)
                button_action($rsc->key, $data_type);
               else
                label_action($rsc->status);
            }
            // <!-- LPPMP -->
            elseif($this->userGroup == 25){
              if ($rsc->status == 1)
                button_action($rsc->key, $data_type);
               else
                label_action($rsc->status);
            }
            // <!-- Wakil Dekan 1 -->
            elseif($this->userGroup == 27){
                if ($rsc->status == 0)
                  button_action($rsc->key, $data_type, ["data-uid" => $rsc->status]);
                else
                  label_action($rsc->status);
            }
            // <!-- SPM -->
            elseif($this->userGroup == 22){
                if ($rsc->status == 11)
                  button_action($rsc->key, $data_type);
                else
                  label_action($rsc->status);
            }
            // <!-- DEKAN -->
            elseif($this->userGroup == 24){
              if ($rsc->status == 2)
                button_action($rsc->key, $data_type, ["data-uid" => $rsc->status]);
              else
                label_action($rsc->status);
            }
            // <!-- Warek 1 -->
            elseif($this->userGroup == 23){
              if ($rsc->status == 3)
                button_action($rsc->key, $data_type, ["data-uid" => $rsc->status]);
              else
                label_action($rsc->status);
            }
          ?>
        </td>
      </tr>
    <?php $no++; endforeach; ?>
  </tbody>
</table>
<script>
$(document).ready(function () {
  $('#example3').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true,
    'scrollX' : true,
  })

  jQuery.fn.tagNameLowerCase = function() {
    return this.prop("tagName").toLowerCase();
  };

  $('[data-toggle="tooltip"]').tooltip({ trigger: 'hover' })
})

$(".info-document").on("click", function(e){
    $("#content").empty();
    $("#content").load("approval-pengabdian/info/" + $(this).attr("data-key"))
  })


  $(".approve").on('click', function (e) {
    let message = null
    let uid = 0;
    if (typeof $(this).attr("data-uid") !== 'undefined') {
      uid++
    }

    if (uid > 0) {
      message = prompt("Catatan untuk dosen")
    }
    
    claim($(this).attr("data-type"), $(this).attr("data-action"), message, true)
  })

  $(".approve-all").on("click", function (e) {
    let arr = []
    let uid = 0;
    $("#example3 tbody tr").each( (index, tr) => {
      if (typeof tr["attributes"]["data-code"] !== 'undefined') {
        let code = tr["attributes"]["data-code"]["value"];
        let tag = $('.column-action-'+code).children().tagNameLowerCase()

        if (tag !== "span") {
          arr.push(code)
        }
      }

    })

    if (arr.length == 0) { 
      alert("Data tidak ditemukan"); 
      return;
    }
    
    claim($(this).attr("data-type"), arr, null, true)
  })

  $(".revisi").on("click", function(e){
    let r = confirm("Yakin ingin merivisi ?")
    if (r) {
      let message = prompt("Catatan untuk dosen ")
      claim($(this).attr("data-type"), $(this).attr("data-action"),message)
    }
  })

  function claim(type,arg, note = null, approve = null) {
    $.ajax({
      method: 'post',
      url: '<?= base_url($_approveURL) ?>',
      data:{ type: type, code: arg, note: note, year: $("#tahunakademik").val(), isApprove: approve},
      beforeSend: function() {
        if (Array.isArray(arg)) {
          for (var i = 0; i < arg.length; i++) {
            $('.column-action-'+arg[i]).html(`<img src="<?= base_url('assets/img/0.gif') ?>" width="10%" />`)
          }
        }else{
          $('.column-action-'+arg).html(`<img src="<?= base_url('assets/img/0.gif') ?>" width="10%" />`)
        }
      },
      success: function(res) {
        res = JSON.parse(res);
        if (res.code !== 200) { alert(res.message); window.location.reload(true); return;}
        if (Array.isArray(arg)) {
          if (res.error) { alert(res.message + " Error : "+res.error)}
          
          for (var i = 0; i < arg.length; i++) {
            $('.column-action-'+arg[i]).html(res.label)
          }
        }else{
         $('.column-action-'+arg).html(res.label)
        }
        
        if (res.disable) {
          $(".approve-all").attr("disabled", true) 
        }
      }
    })
  }
</script>