<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Bukti Pengabdian</h4>
  </div>
  <form class="form-horizontal" action="" method="post">
    <div class="modal-body">
      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th>
                  <th>Program</th>
                  <th>URL</th>
                </tr>
              </thead>
              <tbody>
              <?php $i=1; foreach ($data as $docs) : ?>
                <tr>
                  <td><?php echo $i++ ?></td>
                  <td><?= $docs->judul ?></td>
                  <td><?= $docs->program ?></td>
                  <td>
                      <a href="<?= $docs->url ?>" class="btn btn-xs btn-primary">
                        <?= $docs->url?>
                      </a>&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
    <div class="modal-footer">      
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </form>

</div>
<script>
  $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" })
  })
</script>