<section class="content-header">
  <h1>
    Laporan Penelitian
    <small>Buat laporan penelitian</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=  base_url('/') ?>" data-toggle="tooltip" title="Kembali ke beranda">Dashboard</a></li>
    <li><a href="javascript:void(0);">Laporan Penelitian</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <?php $this->load->view('template/message_alert'); ?>
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-file-text-o"></i>
          <h3 class="box-title">Daftar Penelitian</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-4 col-xs-12">
              <select name="" id="tahunakademik" class="form-control">
                  <option value="" selected="" disabled=""></option>
                  <?php foreach ($years as $year) : ?>
                    <option 
                      <?= $this->activeYear == $year->kode ? 'selected=""' : '' ?>
                      value="<?= $year->kode ?>">
                      <?= $year->tahun_akademik ?>
                    </option>
                  <?php endforeach; ?>
                </select>
            </div>
          </div>
          <div class="form-horizontal">
            
            <span id="content-table">
              <button class="btn btn-primary btn-sm approve-all" <?php echo $disabled ?> data-type="<?php echo $data_type ?>"><i class="fa fa-check-square-o"></i>&nbsp;Approve All</button>
              <table class="table table-bordered display" id="example3" style="width: 100%; margin-top: 15px !important">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Dosen</th>
                    <th>Judul</th>
                    <th>Program</th>
                    <th>Keterangan</th>
                    <th style="width: 120px !important;">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($teaches as $rsc) :
                    $dosen = lecture_data($rsc->nid)->data;
                    $attr = "";

                    if ($this->userGroup == 26 ) { //Kaprodi
                     if ($rsc->status == 8 || is_null($rsc->status)) {
                       $attr = "data-code=".$rsc->key;
                     }
                    }elseif ($this->userGroup == 21) { //GPM
                      if ($rsc->status == 9 ) // Approve By Kaprodi
                        $attr = "data-code=".$rsc->key; 
                    }elseif ($this->userGroup == 27) { //Wakil Dekan 1
                      if ($rsc->status == 0 )  
                        $attr = "data-code=".$rsc->key; 
                    }elseif ($this->userGroup == 22) { //SPM
                      if ($rsc->status == 11) 
                        $attr = "data-code=".$rsc->key;
                    }elseif ($this->userGroup == 23) { //REKTORAT
                      if ($rsc->status == 3) 
                        $attr = "data-code=".$rsc->key;
                    }elseif ($this->userGroup == 24) { //DEKAN
                      if ($rsc->status == 2) 
                        $attr = "data-code=".$rsc->key;
                    }elseif ($this->userGroup == 25) { //LPPMP
                      if($rsc->status == 1)
                        $attr = "data-code=".$rsc->key;
                    }
                  ?>
                    <tr class="action-all" <?php echo $attr ?> >
                      <td><?= $no ?></td>
                      <td><?php echo $dosen->nama. " - ". $dosen->nidn ?></td>
                      <td><?= $rsc->judul ?></td>
                      <td><?= $rsc->program ?></td>
                      <td style="vertical-align: middle;">
                        <?php foreach ($rsc->url as $key => $value): ?>
                          <ul>
                            <li>
                              <a href="<?= $value->url ?>" class="btn btn-xs btn-primary"><?= $value->url?></a>
                            </li>
                          </ul>
                        <?php endforeach ?>
                      </td>
                      <td class="column-action-<?php echo $rsc->key?>" style="vertical-align: middle;" >
                        <?php
                        // <!-- Kaprodi -->
                          if ($this->userGroup == 26){ 
                            if (is_null($rsc->status) || $rsc->status == 8)
                              button_action($rsc->key, $data_type, ["data-uid" => $this->userGroup]);
                            else
                              label_action($rsc->status);
                          }
                          // <!-- GPM -->
                          elseif($this->userGroup == 21){
                            if ($rsc->status == 9)
                              button_action($rsc->key, $data_type);
                             else
                              label_action($rsc->status);
                          }
                          // <!-- LPPMP -->
                          elseif($this->userGroup == 25){
                            if ($rsc->status == 1)
                              button_action($rsc->key, $data_type);
                             else
                              label_action($rsc->status);
                          }
                          // <!-- Wakil Dekan 1 -->
                          elseif($this->userGroup == 27){
                              if ($rsc->status == 0)
                                button_action($rsc->key, $data_type, ["data-uid" => $rsc->status]);
                              else
                                label_action($rsc->status);
                          }
                          // <!-- SPM -->
                          elseif($this->userGroup == 22){
                              if ($rsc->status == 11)
                                button_action($rsc->key, $data_type);
                              else
                                label_action($rsc->status);
                          }
                          // <!-- DEKAN -->
                          elseif($this->userGroup == 24){
                            if ($rsc->status == 2)
                              button_action($rsc->key, $data_type, ["data-uid" => $rsc->status]);
                            else
                              label_action($rsc->status);
                          }
                          // <!-- Warek 1 -->
                          elseif($this->userGroup == 23){
                            if ($rsc->status == 3)
                              button_action($rsc->key, $data_type, ["data-uid" => $rsc->status]);
                            else
                              label_action($rsc->status);
                          }
                        ?>
                      </td>
                    </tr>
                  <?php $no++; endforeach; ?>
                </tbody>
              </table>
            </span>

          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

<div id="myInfo" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <div id="content"></div>

  </div>
</div>

<script>
  $('#tahunakademik').change(function () {
    $.get('<?= base_url('approval-penelitian-pertahun/') ?>' + $(this).val(), {}, function(res) {
      $('#content-table').html(res);
    })
  })

  $(".info-document").on("click", function(e){
    $("#content").empty();
    $("#content").load("approval-penelitian/info/" + $(this).attr("data-key"))
  })


  $(".approve").on('click', function (e) {
    let message = null
    let uid = 0;
    if (typeof $(this).attr("data-uid") !== 'undefined') {
      uid++
    }

    if (uid > 0) {
      message = prompt("Catatan untuk dosen")
    }
    
    claim($(this).attr("data-type"), $(this).attr("data-action"), message, true)
  })

  $(".approve-all").on("click", function (e) {
    let arr = []
    let uid = 0;
    $("#example3 tbody tr").each( (index, tr) => {
      if (typeof tr["attributes"]["data-code"] !== 'undefined') {
        let code = tr["attributes"]["data-code"]["value"];
        let tag = $('.column-action-'+code).children().tagNameLowerCase()

        if (tag !== "span") {
          arr.push(code)
        }
      }

    })

    if (arr.length == 0) { 
      alert("Data tidak ditemukan"); 
      return;
    }
    // console.log(arr)
    claim($(this).attr("data-type"), arr, null, true)
  })

  $(".revisi").on("click", function(e){
    let r = confirm("Yakin ingin merivisi ?")
    if (r) {
      let message = prompt("Catatan untuk dosen ")
      claim($(this).attr("data-type"), $(this).attr("data-action"),message)
    }
  })

  function claim(type,arg, note = null, approve = null) {
    $.ajax({
      method: 'post',
      url: '<?= base_url($_approveURL) ?>',
      data:{ type: type, code: arg, note: note, year: $("#tahunakademik").val(), isApprove: approve},
      beforeSend: function() {
        if (Array.isArray(arg)) {
          for (var i = 0; i < arg.length; i++) {
            $('.column-action-'+arg[i]).html(`<img src="<?= base_url('assets/img/0.gif') ?>" width="10%" />`)
          }
        }else{
          $('.column-action-'+arg).html(`<img src="<?= base_url('assets/img/0.gif') ?>" width="10%" />`)
        }
      },
      success: function(res) {
        res = JSON.parse(res);
        if (res.code !== 200) { alert(res.message); window.location.reload(true); return;}
        if (Array.isArray(arg)) {
          if (res.error) { alert(res.message + " Error : "+res.error)}
          
          for (var i = 0; i < arg.length; i++) {
            $('.column-action-'+arg[i]).html(res.label)
          }
        }else{
         $('.column-action-'+arg).html(res.label)
        }
        
        if (res.disable) {
          $(".approve-all").attr("disabled", true) 
        }
      }
    })
  }

  $(document).ready(function() {
    jQuery.fn.tagNameLowerCase = function() {
      return this.prop("tagName").toLowerCase();
    };
    $('[data-toggle="tooltip"]').tooltip({ trigger: 'hover' })
  })
</script>