<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Bukti Penelitian</h4>
  </div>
  <form class="form-horizontal" action="<?= base_url('submit-doc/'.$doc_key) ?>" method="post">
    <div class="modal-body">
      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis</th>
                  <th>URL</th>
                </tr>
              </thead>
              <tbody>
              <?php $i=1; foreach ($data as $docs) : ?>
                <tr>
                  <td><?php echo $i++ ?></td>
                  <td><?= $docs->jenis ?></td>
                  <?php $is_doc_exist = $this->rrscm->is_research_has_doc($doc_key, $docs->kode);
                  if ($is_doc_exist->num_rows() > 0) : ?>
                    <td>
                      <a href="<?= $is_doc_exist->row()->url ?>" class="btn btn-xs btn-primary">
                        <?= $is_doc_exist->row()->url ?>
                      </a>&nbsp;&nbsp;&nbsp;
                    </td>
                    <?php else : ?>
                    <td>
                      <label for="" class="label label-danger">URL Not Found</label>
                    </td>
                  <?php endif; ?>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
    <div class="modal-footer">      
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </form>

</div>
<script>
  $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" })
  })
</script>