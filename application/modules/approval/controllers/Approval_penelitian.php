<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval_penelitian extends MY_Controller {

	public $username;
	public $userGroup;
	public $userID;
	
	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		if (!$this->session->userdata('bkd_session')) {
			redirect('auth','refresh');
		}
		$this->userID = $this->session->userdata('bkd_session')['userid'];
		$this->username = $this->session->userdata('bkd_session')['username'];
		$this->userGroup = $this->session->userdata('bkd_session')['group'];
	}

	// List all your items
	public function index()
	{
		$this->load->model('approval_model', 'apm');

		$data['years'] = $this->yearList;
		$data['teaches'] =  $this->apm->daftar_penelitian($this->activeYear);
		$data['disabled'] = count($this->apm->daftar_penelitian($this->activeYear, true)) > 0 ? '' : 'disabled=true'; // disable button approve all
		$data['data_type'] = 2;
		$data['pagename'] = "Approval Penelitian";
		$data['page'] = 'approval_penelitian_v';
		$data['_approveURL'] = 'approval-penelitian/approve';

		$this->load->view('template/template', $data);
	}

	public function detail(string $year) : void
	{
		$this->load->model('approval_model', 'apm');

		$data['years'] = $this->yearList;
		$data['teaches'] =  $this->apm->daftar_penelitian($year);
		$data['disabled'] = count($this->apm->daftar_penelitian($year, true)) > 0 ? '' : 'disabled=true'; // disable button approve all
		$data['data_type'] = 2;
		$data['pagename'] = "Approval Penelitian";
		$data['_approveURL'] = 'approval-penelitian/approve';

		$this->load->view('table_list_approval_penelitian_pertahun', $data);		
	}

	public function info($key)
	{
		$this->load->model('penelitian/report_penelitian_model','rrscm');

		$rsc = $this->rrscm->get_research($key);
    	$data['doc_key'] = $rsc->key;
    	$data['data'] = $this->rrscm->proof_doc($rsc->kegiatan, $rsc->param);

		$this->load->view('info_url_penelitian_v', $data);	
	}
	/**
	 * Approval BKD
	 * @param int $id id dari primary key
	 * @return alert 
	 */
	public function approve($value='')
	{
		$type = $this->input->post('type', TRUE);
		
		if (!is_array($this->input->post('code', TRUE))) {
			$kdTransaksi = $this->input->post('code', TRUE);
		}

		$tableName = "";
		$message = "";

		$this->_typeApproval($type, $tableName, $message);
		
		/**
		 * Approval Type 
		 * 1 => Approve
		 * 2 => Revisi
		 * @var $approvalType
		 */
		$approvalType = $this->input->post('isApprove', TRUE) ? 1 : 2;

		$status = approval_status_bkd($approvalType,$this->userGroup);

		$note = empty($this->input->post('note', TRUE)) ? strtolower(approval_status_text($status)) : $this->input->post('note', TRUE);

		$dataInsert = [
			'note' => $note,
			'status'        => $status,
			'updated_at'    => date('Y-m-d H:i:s'),
		];
		
		$this->load->model('approval_model', 'apm');

		/**
		 * Type 
		 * 1 -> Pengajara
		 * 2 -> Penelitian
		 * 3 -> Pengabdian
		 * 4 -> Jabatan Struktural
		 */
		if (is_array($this->input->post('code', TRUE)) && $type == 2) { // Approve All
			
			$dataLog = [];
			$success = 0;
			$failed = 0;

			for ($i=0; $i < count($this->input->post('code', TRUE)); $i++) { 
				$kdTransaksi = $this->input->post('code', TRUE);
				
				$this->data->update($tableName, $dataInsert, ['key' => $kdTransaksi[$i]]);

				$affected = $this->db->affected_rows();

				if ($affected > 0) {
					$success++;
				}else{
					$failed++;
					$note = 'Error Approve By '. $this->userID." Err: ".$this->db->error()['message'];
				}

				$dataLog[] = [ 
					'status' => $status, 
					'kd_transaksi' => $kdTransaksi[$i], 
					'note' => $note, 
					'created_at' => date('Y-m-d H:i:s'), 
					'created_by' => $this->username
				];
			}
			
			create_log($dataLog);

			$resp = [
				'code' => 200, 
				'message' => $message. " Berhasil", 
				'label' => '<span class="label label-'.label_approval($status).'"><i class="fa '.icon_approval($status).'"></i>&nbsp;'. approval_status_text($status) .'</span>'
			];
			
			if ($failed > 0) { 
				$resp["error"] = $failed;
			}

			$row = count($this->apm->daftar_penelitian($this->activeYear, true));

			if ($row == 0) {
				$resp["disable"] = true;
			}

			echo json_encode($resp);
		}else{

			$this->data->update($tableName, $dataInsert, ['key' => $kdTransaksi]);
			$affected = $this->db->affected_rows();

			// jika update sukses
			if ($affected > 0) {

				$data = [ 
					'status' => $status, 
					'kd_transaksi' => $kdTransaksi, 
					'note' => $note, 
					'created_at' => date('Y-m-d H:i:s'), 
					'created_by' => $this->username
				];
				
				create_log($data);

				$resp = [
					'code' => 200, 
					'message' => $message. " Berhasil", 
					'label' => '<span class="label label-'.label_approval($status).'"><i class="fa '.icon_approval($status).'"></i>&nbsp;'. approval_status_text($status) .'</span>'
				];
			}else{
				
				$data = [ 
					'status' => $status, 
					'kd_transaksi' => $kdTransaksi, 
					'note' => 'Error Approve By '. $this->userID,
					'created_at' => date('Y-m-d H:i:s'), 
					'created_by' => $this->username
				];
				
				create_log($data);

				$resp = [
					'code' => 500, 
					'message' => $message. " Gagal", 
				];
			}

			
			$row = count($this->apm->daftar_penelitian($this->activeYear, true)); // check where status is null

			if ($row == 0) {
				$resp["disable"] = true;
			}

			echo json_encode($resp);
		}
	}
}

/* End of file Approval_penelitian.php */
/* Location: ./application/modules/approval/controllers/Approval_penelitian.php */