<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Approval_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Menampikan data pengajaran tambahan dosen untuk kebutuhan approval
	 * @param  int          $year           tahun akademik
	 * @param  bool 		$extraCondition filter status yang hanya butuh approval digunakan untuk status disable tombol approve all
	 * @return Object                       
	 */
	public function get_list_teaching(int $year, bool $extraCondition=false)
	{
		$sql = "SELECT
					ptd.*,
					pt.`komponen`,
					lp.`kode_transaksi` as tsc_rep,
					dbp.`url` as tsc_url,
					'1' as data_type
				FROM
					pengajaran_tambahan_dosen ptd
				JOIN pengajaran_tambahan pt ON
					ptd.`kode_pengajaran` = pt.`kode_pengajaran`
				LEFT JOIN laporan_pengajaran lp ON
					ptd.`kode_transaksi` = lp.`kode_transaksi`
				LEFT JOIN dokumen_bukti_pengajaran dbp ON
					dbp.`kode_transaksi` = lp.`kode_transaksi`
				WHERE
					ptd.`tahunakademik` = '${year}' 
					AND lp.`kode_transaksi` is not null
					AND dbp.`url` is not null
					AND ptd.`deleted_at` is null
				";

		switch ($this->userGroup) {
			case '21':
				$this->_gpm_builder1($sql,$extraCondition);
				break;
			case '22':
				$this->_spm_builder1($sql, $extraCondition);
				break;
			case '23':
				$this->_rektorat_builder1($sql, $extraCondition);
				break;
			case '24':
				$this->_dekanat_builder1($sql, $extraCondition);
				break;
			case '26':
				$this->_kaprodi_builder1($sql, $extraCondition);
				break;
			case '27':
				$this->_wd1_builder1($sql, $extraCondition);
				break;
		}
		
		return $this->db->query($sql);
	}

	/**
	 * Untuk menambahkan filter query khusus untuk kaprodi
	 * dosen yang di filter berdasarkan prodi
	 * @param  string       &$sql           query awal sebelum ditambahkan dengan filter query kaprodi dengan sistem pas by reference
	 * @param  bool 		$extraCondition filter status yang hanya butuh approval digunakan untuk status disable tombol approve all
	 * @return string                       query yang sudah di tambahkan dengan filter query kaprodi
	 */
	function _kaprodi_builder1(&$sql, bool $extraCondition = false) {
		
		$sql .= $this->_lecture_level1($sql);

		if ($extraCondition == true) {
			$sql .= "AND status is null OR status = 8";
		}
	}

	/**
	 * Untuk menambahkan filter query khusus untuk gpm
	 * dosen yang di filter berdasarkan prodi
	 * @param  string       &$sql           query awal sebelum ditambahkan dengan filter query gpm dengan sistem pas by reference
	 * @param  bool 		$extraCondition filter status yang hanya butuh approval digunakan untuk status disable tombol approve all
	 * @return string                       query yang sudah di tambahkan dengan filter query kaprodi
	 */
	function _gpm_builder1(&$sql, bool $extraCondition = false) {
		
		$filter = "SELECT nid from tbl_karyawan where jabatan_id = '$this->userID'";
		
		$list = $this->_list($filter);

		$sql .= " AND `nid` IN (" .$list. ")";

		if ($extraCondition) {
			$sql .= " AND `status` = 9";
		}else{
			$sql .= " AND `status` IS NOT NULL AND `status` != 8";
		}
	}

	/**
	 * Untuk menambahkan filter query khusus untuk Wakil dekan 1
	 * dosen yang di filter berdasarkan fakultas
	 * @param  string       &$sql           query awal sebelum ditambahkan dengan filter query wakil dekan 1 dengan sistem pas by reference
	 * @param  bool 		$extraCondition di gunakan hanya untuk memfilter dengan status yang hanya harus di approve, agar bisa menentukan tombol approve all disable atau tidak
	 * @return string                       query yang sudah di tambahkan dengan filter query wakil dekan 1
	 */
	function _wd1_builder1(&$sql, bool $extraCondition = false) {
		$sql .= $this->_lecture_level2($sql);

		if ($this->userGroup == 27) { // Wakil Dekan 1
			if ($extraCondition) {
				$sql .= " AND status = 1 ";
			}else{
				$sql .= " AND status IS NOT NULL AND status NOT IN (9, 6, 10, 8)";
			}
		}
	}

	/**
	 * Untuk menambahkan filter query khusus untuk SPM
	 * dosen yang di filter berdasarkan fakultas
	 * @param  string       &$sql           query awal sebelum ditambahkan dengan filter query spm dengan sistem pas by reference
	 * @param  bool 		$extraCondition di gunakan hanya untuk memfilter dengan status yang hanya harus di approve, agar bisa menentukan tombol approve all disable atau tidak
	 * @return string                       query yang sudah di tambahkan dengan filter query spm
	 */
	function _spm_builder1(&$sql, bool $extraCondition = false) {
		$sql .= $this->_lecture_level2($sql);

		if ($extraCondition) {
			$sql .= " AND status = 11";
		}else{
			$sql .= " AND status IS NOT NULL AND status NOT IN (1, 9, 6, 10, 8)";
		}
	}

	/**
	 * Untuk menambahkan filter query khusus untuk Wakil Rektor 1
	 * dosen yang di filter berdasarkan fakultas
	 * @param  string       &$sql           query awal sebelum ditambahkan dengan filter query wakil rektor 1 dengan sistem pas by reference
	 * @param  bool 		$extraCondition di gunakan hanya untuk memfilter dengan status yang hanya harus di approve, agar bisa menentukan tombol approve all disable atau tidak
	 * @return string                       query yang sudah di tambahkan dengan filter query wakil rektor 1
	 */
	function _rektorat_builder1(&$sql, bool $extraCondition=false){
		$sql .= $this->_lecture_level3($sql);

		if ($extraCondition == true) {
			$sql .= " AND status IN (3, 14)";
		}	
	}

	/**
	 * Untuk menambahkan filter query khusus untuk Dekan
	 * dosen yang di filter berdasarkan fakultas
	 * @param  string       &$sql           query awal sebelum ditambahkan dengan filter query Dekan dengan sistem pas by reference
	 * @param  bool 		$extraCondition di gunakan hanya untuk memfilter dengan status yang hanya harus di approve, agar bisa menentukan tombol approve all disable atau tidak
	 * @return string                       query yang sudah di tambahkan dengan filter query Dekan
	 */
	function _dekanat_builder1(&$sql, bool $extraCondition=false){
		$sql .= $this->_lecture_level2($sql);	

		if ($extraCondition == true) {
			$sql .= " AND status = 2";
		}	
	}

	/**
	 * Menampikan data penelitian dosen untuk kebutuhan approval
	 * @param  int          $year           tahun akademik
	 * @param  bool 		$extraCondition filter status yang hanya butuh approval digunakan untuk status disable tombol approve all
	 * @return Array     
	 */
	public function daftar_penelitian(int $tahunakademik, bool $extraCondition=false) 
	{
		$sql = "SELECT DISTINCT
						pd.`key`,
						pd.`judul`,
						pd.`sks`,
						pd.`nid`,
						prg.`program`,
						act.`kegiatan`,
						prm.`parameter`,
						pd.`status`,
						pd.`note`,
						pd.`tahunakademik`,
						(SELECT COUNT(*) FROM dokumen_penelitian 
						WHERE kode_kegiatan = pd.`kegiatan` 
						AND kode_param = pd.`param`) AS requirement,
						(SELECT COUNT(*) FROM bukti_penelitian
						WHERE key_penelitian = pd.`key`
						AND deleted_at IS NULL) AS attachment,
						'' as url
				FROM penelitian_dosen pd
				JOIN program_penelitian prg ON prg.`kode_program` = pd.`program`
				JOIN kegiatan_penelitian act ON act.`kode_kegiatan` = pd.`kegiatan`
				JOIN persentase_param_penelitian prm ON prm.`kode_param` = pd.`param`
				WHERE pd.`tahunakademik` = '{$tahunakademik}'
				AND pd.`deleted_at` IS NULL ";

		switch ($this->userGroup) {
			case '26':
				$this->_kaprodi_builder2($sql, $extraCondition); // Kaprodi
				break;
			case '21':
				$this->_gpm_builder2($sql, $extraCondition); // GPM
				break;
			case '25':
				$this->_lppmp_builder2($sql, $extraCondition); // LPPMP
				break;
			case '27':
				$this->_wd1_builder2($sql, $extraCondition); // Wakil Dekan 1
				break;
			case '22':
				$this->_spm_builder2($sql, $extraCondition); // SPM
				break;
			case '24':
				$this->_dekanat_builder2($sql, $extraCondition); // dekanat
				break;
			case '23':
				$this->_rektorat_builder2($sql, $extraCondition); // rektorat
				break;
		}

		$sql .= " HAVING requirement = attachment";	 // jumlah file yang dibutuhkan dengan file yang di unggah sama
		$sql .= " ORDER BY pd.`tahunakademik` ASC";

		$data = $this->db->query($sql)->result();
		
		foreach ($data as $key => $value) {
			$this->db->select("url");
			$bp = $this->db->get_where('bukti_penelitian', ['key_penelitian' => $value->key])->result();
			$data[$key]->url = $bp; 
		}

		return $data;
	}

	function _kaprodi_builder2(&$sql, $extraCondition = false){
		$sql .= $this->_lecture_level1($sql);

		$this->_lecture_level1($sql);

		if ($extraCondition) {
			$sql .= " AND status is NULL OR status = 8";
		}
	}

	function _gpm_builder2(&$sql, $extraCondition = false){
		$sql .= $this->_lecture_level1($sql);

		if ($extraCondition) {
			$sql .= " AND status = 9";
		}else{
			$sql .= " AND  status IS NOT NULL AND status != 8";
		}
	}

	function _lppmp_builder2(&$sql, $extraCondition = false){
		$sql .= $this->_lecture_level3($sql);

		if ($extraCondition) {
			$sql .= " AND status = 1";
		}else{
			$sql .= " AND status IS NOT NULL AND status NOT IN (9,6,10,8)";
		}
	}

	function _wd1_builder2(&$sql, $extraCondition = false){
		$sql .= $this->_lecture_level2($sql);

		if ($extraCondition) {
			$sql .= " AND status = 0 ";
		}else{
			$sql .= " AND status IS NOT NULL AND status NOT IN (9, 1, 6, 10, 8, 5)";
		}
	}

	function _spm_builder2(&$sql, $extraCondition = false){
		$sql .= $this->_lecture_level2($sql);

		if ($extraCondition) {
			$sql .= " AND status = 11";
		}else{
			$sql .= " AND status IS NOT NULL AND status NOT IN (9, 1, 0, 5, 6, 10, 12, 8)";
		}
	}

	function _dekanat_builder2(&$sql, $extraCondition = false){
		if ($extraCondition) {
			$sql .= " AND status IS NOT NULL AND status = 2 ";
		}
	}

	function _rektorat_builder2(&$sql, $extraCondition = false){
		if ($extraCondition) {
			$sql .= " AND status IS NOT NULL AND status = 3 ";
		}
	}

	/**
	 * Get list lecturer
	 * 
	 * @return object
	 */
	public function get_struktural(int $tahunakademik, bool $extraCondition=false)
	{
		$sql = "SELECT 
					sd.`_key` as kode_transaksi, 
					sd.`nid`, 
					sd.`status`, 
					js.`jabatan` as komponen, 
					bj.`url` as tsc_url,
					'4' as data_type,
					sd.`deleted_at`
				FROM struktural_dosen sd
				JOIN jabatan_struktural js ON js.`kode` = sd.`kode_jabatan`
				LEFT JOIN bukti_jabatan bj ON bj.`_key` = sd.`_key`
				WHERE sd.`tahunakademik` = '{$tahunakademik}' AND bj.`url` is not null";
		
		switch ($this->userGroup) {
			case '21':
				$this->_gpm_builder1($sql,$extraCondition);
				break;
			case '22':
				$this->_spm_builder1($sql, $extraCondition);
				break;
			case '23':
				$this->_rektorat_builder1($sql, $extraCondition);
				break;
			case '24':
				$this->_dekanat_builder1($sql, $extraCondition);
				break;
			case '26':
				$this->_kaprodi_builder1($sql, $extraCondition);
				break;
			case '27':
				$this->_wd1_builder1($sql, $extraCondition);
				break;
		}
		
		$data = $this->db->query($sql);
		return $data;
	}

	function gen_list($data){
		$list = "";
		
		for ($i=0; $i < count($data); $i++) { 
			yield $data[$i];
		}
	}

	function _list($data){
		$list = null;

		$dosen = lecture_list($data);
		
		$list = "";

		$generator = $this->gen_list($dosen->data);

		foreach ($generator as $value) {
			$list .= "'".$value."',";
		}

		$list = substr($list, 0, -1);
		return $list;
	}

	/**
	 * Mendapatkan dosen berdasarkan prodi
	 * @param  string 	&$sql 
	 * @return string 	query yang sudah di tambahkan filter dosen
	 */
	function _lecture_level1(&$sql) {
		$filter = "SELECT nid from tbl_karyawan where jabatan_id = '$this->userID'";
		
		$list = $this->_list($filter);

		$sql .= " AND `nid` IN (" .$list. ")";
	}

	/**
	 * Mendapatkan dosen berdasarkan fakultas
	 * @param  string 	&$sql 
	 * @return string 	query yang sudah di tambahkan filter dosen
	 */
	function _lecture_level2(&$sql){
		$filter = "SELECT 
							nid 
					FROM tbl_karyawan 
						WHERE 
							jabatan_id IN (
								SELECT kd_prodi 
									FROM tbl_jurusan_prodi 
										WHERE kd_fakultas = '$this->userID'
									)";
		
		$list = $this->_list($filter);

		$sql .= " AND `nid` IN (" .$list. ")";
	}

	/**
	 * Mendapatkan dosen untuk seluruh fakultas dan prodi
	 * @param  string 	&$sql 
	 * @return string 	query yang sudah di tambahkan filter dosen
	 */
	function _lecture_level3(&$sql)
	{
		$filter = "SELECT nid FROM tbl_karyawan ";
		
		$list = $this->_list($filter);

		$sql .= " AND `nid` IN (" .$list. ")";	
	}

	public function get_devotions($tahunakademik, bool $extraCondition=false)
	{
		$sql = "SELECT 
					ad.`_key` as 'key',
					ad.`status`,
					ad.`nid`,
					ab.`nama` as judul,
					ap.`program`,
					pr.`nama` AS peran,
					bk.`url`
				FROM abdimas_dosen ad
				JOIN abdimas ab ON ab.`kode` = ad.`type`
				JOIN abdimas_program ap ON ap.`kode_program` = ad.`program`
				LEFT JOIN abdimas_parameter pr ON pr.`kode` = ad.`param`
				JOIN bukti_pengabdian bk ON ad.`_key` = bk.`_key`
				WHERE ad.`tahunakademik` = '{$tahunakademik}'
				AND ad.`deleted_at` IS NULL";

		switch ($this->userGroup) {
			case '26':
				$this->_kaprodi_builder2($sql, $extraCondition); // Kaprodi
				break;
			case '21':
				$this->_gpm_builder2($sql, $extraCondition); // GPM
				break;
			case '25':
				$this->_lppmp_builder2($sql, $extraCondition); // LPPMP
				break;
			case '27':
				$this->_wd1_builder2($sql, $extraCondition); // Wakil Dekan 1
				break;
			case '22':
				$this->_spm_builder2($sql, $extraCondition); // SPM
				break;
			case '24':
				$this->_dekanat_builder2($sql, $extraCondition); // SPM
				break;
			case '23':
				$this->_rektorat_builder2($sql, $extraCondition); // SPM
				break;
		}

		$data = $this->db->query($sql)->result();
		return $data;
	}

	public function get_devotions_by_key($key, $tahunakademik)
	{
		$sql = "SELECT 
					ad.`_key` as 'key',
					ad.`status`,
					ad.`nid`,
					ab.`nama` as judul,
					ap.`program`,
					pr.`nama` AS peran,
					bk.`url`
				FROM abdimas_dosen ad
				JOIN abdimas ab ON ab.`kode` = ad.`type`
				JOIN abdimas_program ap ON ap.`kode_program` = ad.`program`
				LEFT JOIN abdimas_parameter pr ON pr.`kode` = ad.`param`
				JOIN bukti_pengabdian bk ON ad.`_key` = bk.`_key`
				WHERE ad.`tahunakademik` = '{$tahunakademik}'
				AND ad.`_key` = '{$key}'
				AND ad.`deleted_at` IS NULL";

		$data = $this->db->query($sql)->result();
		return $data;
	}
}

/* End of file approval_model.ph */
/* Location: ./application/models/approval_model.ph */