<form id="form-struktural" role="form" action="<?= base_url('update-struktural/'.$id) ?>" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Ubah Data Struktural</h4>
  </div>
  <div class="modal-body">
    <div class="box-body">
      <div class="form-group">
        <label for="position" class="col-sm-4 control-label">Jabatan Struktural</label>
        <div class="col-sm-8">
          <select name="position" id="position" class="form-control" required>
            <?php foreach ($jabatan as $other) { ?>
              <option value="<?= $other->id.'-'.$other->sks ?>" <?= $other->id == $sid_jabatan ? 'selected="true"': "" ?> ><?= $other->jabatan ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <br><br>
      <div class="form-group">
        <label for="sks" class="col-sm-4 control-label">Beban SKS</label>
        <div class="col-sm-8">
          <input type="text" name="sks" readonly="" value="<?php echo $ssks ?>" id="sks" class="form-control" placeholder="SKS">
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
</form>

<script>
  // set sks to input box
  $('#position').change(function() {
    var credits = $(this).val().split("-")[1];
    $('#sks').val(credits);
  })

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }
  

  $(document).ready(function () {
    $("#form-struktural").submit(function(e){
      e.preventDefault()
      if ($("#tahunakademik").val() == null) {
        alert('Pilih salah satu tahun akademik terlebih dahulu!'); 
        return false;
      }


      $("<input />").attr("type", "hidden")
            .attr("name", "tahunakademik")
            .attr("value", $("#tahunakademik").val())
            .appendTo("#form-struktural");

      $("#form-struktural")[0].submit();
    })
  })
</script>