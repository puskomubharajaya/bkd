<section class="content-header">
  <h1>
    Jabatan Struktural
    <small>Kelola Jabatan Struktural</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=  base_url('/') ?>" data-toggle="tooltip" title="Kembali ke beranda">Dashboard</a></li>
    <li><a href="javascript:void(0);">Jabatan Struktural</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <?php $this->load->view('template/message_alert'); ?>
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-file-text-o"></i>
          <h3 class="box-title">Daftar Jabatan Struktural</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="form-horizontal">
            
            <div class="form-group">
              <label for="tahunakademik" class="control-label col-sm-2">Tahun Akademik</label>
              <div class="col-sm-8">
                <select name="" id="tahunakademik" class="form-control">
                  <option value="" selected="" disabled=""></option>
                  <?php foreach ($years as $year) : ?>
                    <option 
                      <?= $this->activeYear == $year->kode ? 'selected=""' : '' ?>
                      value="<?= $year->kode ?>">
                      <?= $year->tahun_akademik ?>
                    </option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-sm-2">
                <button class="btn btn-default" data-target="#myInfo" data-toggle="modal">
                  <i class="fa fa-lightbulb-o"></i> Petunjuk</button>
              </div>
            </div>
            
            <span id="content-table">
              <table class="table table-bordered" id="dt-wo-hd">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Jabatan</th>
                    <th>Keterangan</th>
                    <th>Note</th>
                    <th style="text-align: center;">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(empty($poss)): ?>
                    <tr>
                      <td colspan="5" align="center">No data available</td>
                    </tr>
                  <?php endif?>
                  <?php $no=1; foreach ($poss as $pos) : ?>
                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $pos->jabatan ?></td>
                      <td style="vertical-align: middle;">
                        <?= !is_null($pos->url) && is_null($pos->deleted_at)
                            ? '<a class="btn btn-xs bg-green" href="'.$pos->url.'" target="_blank">Dokumen telah dilampirkan <i class="fa fa-external-link"></i></a>'
                            : '<a style="cursor: text" class="btn btn-xs btn-default">Dokumen belum dilampirkan</a>'; ?>
                      </td>
                      <td><?php echo $pos->note ?></td>
                      <td style="vertical-align: middle;" width="120">
                        <?php if (in_array($pos->status, unserialize(REVITION_STATUS))): ?>
                          <span data-toggle="tooltip" title="ubah">
                            <button 
                              type="button" 
                              class="btn btn-warning" 
                              data-toggle="modal" 
                              data-target="#modal-edit"
                              onclick="edit(<?= $pos->id ?>)">
                              <i class="fa fa-pencil"></i>
                            </button>
                          </span>
                        <?php elseif(is_null($pos->status)): ?>
                          <span data-toggle="tooltip" title="ubah">
                            <button 
                              type="button" 
                              class="btn btn-warning" 
                              data-toggle="modal" 
                              data-target="#modal-edit"
                              onclick="edit(<?= $pos->id ?>)">
                              <i class="fa fa-pencil"></i>
                            </button>
                          </span>
                          <a 
                            onclick="return confirm('Yakin ingin menghapus data ini?')" 
                            href="<?=  base_url('hapus-struktural/'.$pos  ->id) ?>" 
                            class="btn btn-danger" 
                            data-toggle="tooltip" 
                            title="hapus">
                            <i class="fa fa-trash"></i>
                          </a>
                        <?php endif ?>
                      </td>
                    </tr>
                  <?php $no++; endforeach; ?>
                </tbody>
              </table>
            </span>

          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

<div id="modal-edit" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content" id="content">
      
    </div>
  </div>
</div>

<script>
  $('#tahunakademik').change(function () {
    $.get('<?= base_url('struktural-pertahun/') ?>' + $(this).val(), {}, function(res) {
      $('#content-table').empty().html(res);
    })
  })

  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({ trigger: 'hover' })
  })
  
  function edit(id) {
    $('#content').load('<?= base_url('ubah-struktural/') ?>' + id);
  }

  
</script>