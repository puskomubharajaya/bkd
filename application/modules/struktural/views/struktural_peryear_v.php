<span id="content-table">
  <table class="table table-bordered" id="dt-wo-hd">
    <thead>
      <tr>
        <th>No</th>
        <th>Jabatan</th>
        <th>Keterangan</th>
        <th>Note</th>
        <th style="text-align: center;">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php if(empty($poss)): ?>
        <tr>
          <td colspan="5" align="center">No data available</td>
        </tr>
      <?php endif?>
        
      <?php $no=1; foreach ($poss as $pos) : ?>
      <tr>
        <td><?= $no ?></td>
        <td><?= $pos->jabatan ?></td>
        <td style="vertical-align: middle;">
          <?= !is_null($pos->url) && is_null($pos->deleted_at)
            ? '<a class="btn btn-xs bg-green" href="'.$pos->url.'" target="_blank">Dokumen telah dilampirkan <i class="fa fa-external-link"></i></a>'
            : '<a style="cursor: text" class="btn btn-xs btn-default">Dokumen belum dilampirkan</a>'; ?>
        </td>
        <td><?php echo $pos->note ?></td>
        <td style="vertical-align: middle;" width="120">
          <?php if (in_array($pos->status, unserialize(REVITION_STATUS))): ?>
            <span data-toggle="tooltip" title="<?= $year != $active_year ? 'Data lampu tidak dapat diubah!' : 'Ubah'; ?>">
              <button
              type="button"
              class="btn btn-warning"
              data-toggle="modal"
              data-target="#modal-edit"
              <?= $year != $active_year ? 'disabled=""' : ''; ?>
              onclick="edit(<?= $pos->id ?>)">
              <i class="fa fa-pencil"></i>
              </button>
            </span>
          <?php elseif(is_null($pos->status)): ?>
            <span data-toggle="tooltip" title="<?= $year != $active_year ? 'Data lampu tidak dapat diubah!' : 'Ubah'; ?>">
              <button
              type="button"
              class="btn btn-warning"
              data-toggle="modal"
              data-target="#modal-edit"
              <?= $year != $active_year ? 'disabled=""' : ''; ?>
              onclick="edit(<?= $pos->id ?>)">
              <i class="fa fa-pencil"></i>
              </button>
            </span>
            <a
              <?php if ($year == $active_year) : ?>onclick="return confirm('Yakin ingin menghapus data ini?')"<?php endif; ?>
              href="<?= $year != $active_year ? 'javascript:void(0)' : base_url('hapus-struktural/'.$pos  ->id) ?>"
              class="btn btn-danger"
              data-toggle="tooltip"
              <?= $year != $active_year ? 'disabled=""' : '' ?>
              title="<?= $year != $active_year ? 'Tidak dapat menghapus data lampau!' : 'Hapus' ?>">
              <i class="fa fa-trash"></i>
            </a>
          <?php endif ?>
        </td>
      </tr>
      <?php $no++; endforeach; ?>
    </tbody>
  </table>
</span>


<script>
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({ trigger: 'hover' })
  })

  function edit(id) {
    $('#content').load('<?= base_url('ubah-struktural/') ?>' + id);
  }
</script>