<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Struktural_model extends MY_Model {

	protected $_table = 'struktural_dosen';

	function __construct()
	{
		parent::__construct();
	}

	public function get_struktural(string $nid, int $tahunakademik)
	{
		$data = $this->db->query("SELECT 
									sd.`id`, 
									sd.`_key`, 
									sd.`status`, 
									js.`jabatan`,
									bj.`url`, 
									sd.`note`, 
									sd.`deleted_at`, 
									sd.`note` 
								FROM struktural_dosen sd
								JOIN jabatan_struktural js ON js.`kode` = sd.`kode_jabatan`
								LEFT JOIN bukti_jabatan bj ON bj.`_key` = sd.`_key`
								WHERE sd.`nid` = '{$nid}'
								AND sd.`tahunakademik` = '{$tahunakademik}' 
								AND sd.`deleted_at` IS NULL");
		return $data;
	}

	
}

/* End of file Struktural_model.php */
/* Location: ./application/models/Struktural_model.php */