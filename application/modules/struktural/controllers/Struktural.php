<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Struktural extends MY_Controller {

	protected $username;
	protected $userid;
	protected $table = 'struktural_dosen';

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		if (!$this->session->userdata('bkd_session')) {
			redirect('auth','refresh');
		}
		$this->username = $this->session->userdata('bkd_session')['username'];
		$this->userid = $this->session->userdata('bkd_session')['userid'];

	}

	// List all your items
	public function index( $offset = 0 )
	{
		$keys = array_search($this->activeYear, array_map(function($v){return $v->kode;},$this->yearList));
		
		$list = array_splice($this->yearList, $keys - 3);

		$data['years'] = $list;
		$data['username']  = $this->username;
		$data['pagename']  = 'Dashboard';

		$jabatan = $this->data->findAll('jabatan_struktural');

		$data['jabatan'] = $jabatan->result();
		$data['page'] = 'struktural_v';
		
		$this->load->view('template/template', $data);
	}

	// Add a new item
	public function store()
	{
		$req = explode("-", $_POST['position']);
		$position = $req[0];
		$sks = $req[1];
		$kode = $req[2];
		$tahunakademik = $_POST['tahunakademik'];

		$condition = ['nid' => $this->userid, "tahunakademik" => $tahunakademik, 'deleted_at IS NULL' => NULL];
		$exist = $this->data->find($this->table, $condition)->num_rows(); // check exist data before insert
		
		$data = [
			'_key' => $this->_generateRandomString(),
			'nid' => $this->userid,
			'id_jabatan' => $position,
			'kode_jabatan' => $kode,
			'tahunakademik' => $tahunakademik,
			'sks' => $sks,
			'note'=> "",
			'created_at' => date('Y-m-d H:i:s'),
		];

		if ($exist) {
			$res = $this->data->update($this->table, $data, $condition);
		}else{
			$res = $this->data->insert($this->table, $data);
		}
		
		if ($res) {
			$this->session->set_flashdata('success', 'Jabatan struktural berhasil disimpan!');
	    	redirect('struktural','refresh');
		}else{
			$this->session->set_flashdata('success', 'Jabatan struktural gagal disimpan!');
	    	redirect('struktural','refresh');
		}
	}

	public function show()
	{
		$this->load->model('struktural_model', 'struktur');
		$data['years'] = $this->yearList;
		$data['poss'] = $this->struktur->get_struktural($this->userid, $this->activeYear)->result();
		$data['pagename'] = 'Laporan Jabatan Struktural';
		$data['page'] = 'struktural_detail_v';
		$this->load->view('template/template', $data);
	}

	public function review($tahunakademik)
	{
		$data['year'] = $tahunakademik;
		$data['active_year'] = $this->activeYear;
		$this->load->model('struktural_model', 'struktur');
		$data['poss'] = $this->struktur->get_struktural($this->userid, $tahunakademik)->result();
		$this->load->view('struktural_peryear_v', $data);
	}

	public function edit($id)
	{
		$data['id'] = $id;
		$data['jabatan'] = $this->data->findAll('jabatan_struktural')->result();
		
		$oldStruct = $this->data->find($this->table, ['id' => $id])->row();
		$data['skode'] = $oldStruct->id;
		$data['ssks'] = $oldStruct->sks;
		$data['sid_jabatan'] = $oldStruct->id_jabatan;
		$this->load->view('modal_edit_struktural', $data);
	}

	public function update($id)
	{
		$this->load->model('struktural_model', 'struktural');
		$exist = $this->data->isExist($this->table, ['id' => $id]);
		
		if (!$exist) {
			$this->session->set_flashdata('fail', 'Gagal mengubah data ID tidak ditemukan');
			redirect(base_url('daftar-struktural'));
		}else{
			$id_jabatan = substr($this->input->post('position', TRUE), 0, -2);
		
			$kode_jabatan = $this->data->find('jabatan_struktural', ['id' => $id_jabatan])->row()->kode;
		
			$sks = $this->input->post('sks', TRUE);

			$key = $this->struktural->find(['nid' => $this->userid])->row()->_key;
		
			$dataUpdate = [
				'id_jabatan' => $id_jabatan,
				'sks' => $sks,
				'kode_jabatan' => $kode_jabatan,
				'status' =>  8, // Revisi Dosen
				'tahunakademik' => $this->input->post('tahunakademik', TRUE),
				'updated_at' => date('Y-m-d H:i:s'),
			];
			
			create_log([
				'kd_transaksi' => $key,
				'status' => 8, 
				'note' => approval_status_text(8),
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => $this->username,
			]);

			$this->db->update($this->table, $dataUpdate, ['id' => $id]);
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			redirect(base_url('daftar-struktural'));
		}
	}
	public function remove( $id = NULL )
	{
		$exist = $this->data->find($this->table, $id);
		$this->_is_has_report($exist->row()->_key);

		if ($exist->num_rows()) {
			$this->db->update($this->table, ['deleted_at' => date('Y-m-d H:i:s')], ['id' => $id]);
			$this->session->set_flashdata('success', 'Jabatan struktural berhasil dihapus !');
	    	redirect('struktural','refresh');
		}else{
			$this->session->set_flashdata('danger', 'Jabatan struktural gagal dihapus !');
	    	redirect('struktural','refresh');
		}
	}

	/**
	 * Check whether data has a report of completeness
	 * 
	 * @param int $id
	 * @return void
	 */
	protected function _is_has_report(string $key) : void
	{
		$get_key = $this->db->get_where('bukti_jabatan', ['_key' => $key]);
		if ($get_key->num_rows() > 0 && is_null($get_key->row()->deleted_at)) {
			$this->session->set_flashdata('fail', 'Tidak dapat menghapus data! Jabatan memiliki laporan selesai.');
	    	redirect('struktural','refresh');			
		}
		return;
	}

	/**
     * Generate random string
     * 
     * @return string
     */
    protected function _generateRandomString() : string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

/* End of file Struktural.php */
/* Location: ./application/modules/struktural/controllers/Struktural.php */
